build:
	go get github.com/aws/aws-lambda-go/events
	go get github.com/aws/aws-lambda-go/lambda
	env GOOS=linux GOARCH=amd64 go build -ldflags="-s -w" -o bin/lambda_custom lambda_custom/main.go
	env GOOS=linux GOARCH=amd64 go build -ldflags="-s -w" -o bin/lambda_proxy lambda_proxy/main.go
