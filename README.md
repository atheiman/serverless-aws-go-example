# Serverless AWS Go Example

```shell
# compile the go code into bin/
make

# deploy the service
sls deploy -v
# grab the "ServiceEndpoint" from the end of the output

curl '<ServiceEndpoint>/lambda-custom'
# returns a json response

curl -d "{\"request sent at\": \"$(date)\"}" '<ServiceEndpoint>/lambda-proxy/1/2/3/4?query=string'
# the POST data is logged to cloudwatch and then also returned in an http response

# destroy the service
sls remove -v
```
