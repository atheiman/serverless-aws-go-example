package main

import (
    "fmt"

	"github.com/aws/aws-lambda-go/lambda"
)

type Response struct {
	Message string `json:"messageFromHandlerFunction"`
}

func Handler() (Response, error) {
    fmt.Println("lambda_custom received request")
	return Response{
		Message: "Message within response body json from lambda_custom lambda function",
	}, nil
}

// // very simple, returns just a string
// func Handler() (string, error) {
//     fmt.Println("lambda_custom received request")
//     return "Hello!", nil
// }

func main() {
	lambda.Start(Handler)
}
